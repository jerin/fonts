Fonts
===
This is a few select fonts from the google fonts repository on GitHub. The fonts included are:
 
* arvo
* gentiumbookbasic
* josefinsans
* josefinslab
* lato
* librebaskerville
* lora
* merriweather
* merriweathersans
* notosans
* notoserif
* oldstandardtt
* opensans
* playfairdisplay
* ptsans
* ptserif
* roboto
* sourcecodepro
* sourcesanspro
* sourceserifpro
* vollkorn
