import subprocess as s
import sys

link = "https://github.com/google/fonts/trunk/"
licenses = ['ofl', 'apache']
directory = sys.argv[1]

for l in licenses:
    font_list = open(l+'.txt', "r")
    for font in font_list:
        dl_link = link+l+'/'+font.strip('\n')
        path = directory+'/'+font.strip('\n')
        s.call(["svn", "export",  dl_link, path])
