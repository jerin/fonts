if [ ! -d "post-download" ]; then
	mkdir post-download
fi
python2 download.py post-download
if [ ! -d "~/.fonts" ] ; then
	mkdir ~/.fonts
fi
find ./ -name "*.ttf" | xargs -L 1 -I % cp % ~/.fonts/
